# information

Author: Susie

## Description

Files can always be changed in a secret way. Can you find the flag?
[cat.jpg](./cat.jpg)

## Solve

I used `strings` on the file and saw there was a sus string for the license:

```shell
$ strings ./cat.jpg | head -n 13

JFIF
0Photoshop 3.0
8BIM
PicoCTF
http://ns.adobe.com/xap/1.0/
<?xpacket begin='
' id='W5M0MpCehiHzreSzNTczkc9d'?>
<x:xmpmeta xmlns:x='adobe:ns:meta/' x:xmptk='Image::ExifTool 10.80'>
<rdf:RDF xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'>
 <rdf:Description rdf:about=''
  xmlns:cc='http://creativecommons.org/ns#'>
  <cc:license rdf:resource='cGljb0NURnt0aGVfbTN0YWRhdGFfMXNfbW9kaWZpZWR9'/>
 </rdf:Description>
```

The string `cGljb0NURnt0aGVfbTN0YWRhdGFfMXNfbW9kaWZpZWR9` is base64 encoded from
the string `picoCTF{the_m3tadata_1s_modified}`.

## Flag

`picoCTF{the_m3tadata_1s_modified}`
