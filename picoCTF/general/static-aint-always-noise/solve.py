import re
import subprocess
from subprocess import CompletedProcess

FLAG_RE = re.compile(r'picoCTF{.*}')

def solve() -> str:
    # Use the included shell script to solve the challenge.
    # The script will write the flag to a file 'static.ltdis.strings.txt'
    process: CompletedProcess = subprocess.run(['strings', '-a', '-t', 'x', 'static'], check=True, capture_output=True)
    # Search for the flag in stdout using re.
    try:
        flag: str = FLAG_RE.search(process.stdout.decode()).group(0)
    except AttributeError as e:
        raise RuntimeError('Could not find flag in output') from e
    return flag

if __name__ == '__main__':
    flag: str = solve()
    print(f'[+] Flag: {flag}')